package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

const statusMsg = `Sealed: %t
Key Shares: %d
Key Threshold: %d
Version: %s
Cluster ID: %s
`

func init() {
	rootCmd.AddCommand(statusCmd)
}

var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Prints the current state of Social Specters including whether it is sealed",
	Long:  `Prints the current state of Social Specters including whether it is sealed.`,
	Run: func(cmd *cobra.Command, args []string) {
		reply, err := spectersClient.Status(context.Background())
		if err != nil {
			Fatal(fmt.Errorf("status %v", err))
		}

		fmt.Printf(
			statusMsg,
			reply.Sealed,
			reply.Shares,
			reply.Threshold,
			reply.Version,
			reply.ClusterID,
		)

		if reply.Sealed {
			os.Exit(2)
		}
	},
	Args: cobra.NoArgs,
}
