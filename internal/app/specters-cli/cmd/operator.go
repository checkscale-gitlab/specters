package cmd

import (
	"context"
	"crypto/rand"
	"fmt"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/spf13/cobra"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"golang.org/x/crypto/ssh/terminal"
	"strings"
)

const initMsg = `Initial Root Key: %[1]s

Social Specters initialized with %[2]d key shares and a key threshold of %[3]d. Please securely
distribute the key shares printed above. When the Social Specters is re-sealed,
restarted, or stopped, you must supply at least %[3]d of these keys to unseal it
before it can start servicing requests.

Social Specters does not store the generated master key. Without at least %[3]d key to
reconstruct the master key, Social Specters will remain permanently sealed!
`

const unsealMsg = `Initialized        %t
Sealed             %t
Total Shares       %d
Threshold          %d
Unseal Progress    %d/%d
Version            %s
`

func init() {
	rootCmd.AddCommand(operatorCmd)
	operatorCmd.AddCommand(initCmd)
	operatorCmd.AddCommand(unsealCmd)
}

var operatorCmd = &cobra.Command{
	Use:   "operator",
	Short: "Groups subcommands for operators interacting with Social Specters",
	Long:  `Groups subcommands for operators interacting with Social Specters`,
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize Social Specters server",
	Long:  `Initialization is the process by which Social Specters' storage backend is prepared to receive data.`,
	Run: func(cmd *cobra.Command, args []string) {
		privKey, _, err := crypto.GenerateEd25519Key(rand.Reader)
		if err != nil {
			Fatal(fmt.Errorf("error creating root key - %v", err))
		}
		identity := specters.NewLibP2PIdentity(privKey)
		privKeyBinary, _ := identity.MarshalBinary()
		pubKeyBinary, _ := identity.GetPublic().MarshalBinary()

		reply, err := spectersClient.Init(context.Background(), pubKeyBinary)
		if err != nil {
			Fatal(fmt.Errorf("init %v", err))
		}

		for i, k := range reply.Keys {
			fmt.Printf("Unseal Key %v: %v", i, specters.EncodeKey(k))
			fmt.Println()
		}
		fmt.Println()
		fmt.Printf(initMsg, specters.EncodeKey(privKeyBinary), len(reply.Keys), reply.Threshold)
	},
	Args: cobra.NoArgs,
}

var unsealCmd = &cobra.Command{
	Use:   "unseal [unseal key]",
	Short: "Unseal Social Specters server",
	Long:  `Allows the user to provide a portion of the master key to unseal a Social Specters server.`,
	Run: func(cmd *cobra.Command, args []string) {
		unsealKey := ""

		if len(args) > 0 {
			unsealKey = strings.TrimSpace(args[0])
		}

		if unsealKey == "" {
			fmt.Print("Unseal Key (will be hidden): ")
			value, err := terminal.ReadPassword(0)
			fmt.Println()
			fmt.Println()
			if err != nil {
				Fatal(fmt.Errorf("error getting unseal key - %v", err))
			}

			unsealKey = strings.TrimSpace(string(value))
		}

		decoded, err := specters.DecodeKey(unsealKey)
		if err != nil {
			Fatal(fmt.Errorf("error decoding unseal key - %v", err))
		}
		reply, err := spectersClient.Unseal(context.Background(), decoded)
		if err != nil {
			Fatal(fmt.Errorf("unseal %v", err))
		}

		fmt.Printf(
			unsealMsg,
			reply.Initialized,
			reply.Sealed,
			reply.Shares,
			reply.Threshold,
			reply.Progress,
			reply.Threshold,
			reply.Version,
		)
	},
	Args: cobra.MaximumNArgs(1),
}
