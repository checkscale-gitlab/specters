package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"golang.org/x/crypto/ssh/terminal"
	"strings"
)

const loginMsg = `Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "specters-cli login"
again. Future Social Specters requests will automatically use this token.

token: %s
`

func init() {
	rootCmd.AddCommand(loginCmd)
}

var loginCmd = &cobra.Command{
	Use:   "login [key]",
	Short: "Authenticates users to Social Specters using the provided arguments",
	Long:  `Authenticates users to Social Specters using the provided arguments.`,
	Run: func(cmd *cobra.Command, args []string) {
		userKey := ""

		if len(args) > 0 {
			userKey = strings.TrimSpace(args[0])
		}

		if userKey == "" {
			fmt.Print("User Key (will be hidden): ")
			value, err := terminal.ReadPassword(0)
			fmt.Println()
			fmt.Println()
			if err != nil {
				Fatal(fmt.Errorf("error getting unseal key - %v", err))
			}

			userKey = strings.TrimSpace(string(value))
		}

		decoded, err := specters.DecodeKey(userKey)
		if err != nil {
			Fatal(fmt.Errorf("error decoding user key - %v", err))
		}

		identity, err := specters.UnmarshalLibP2PIdentity(decoded)
		if err != nil {
			Fatal(fmt.Errorf("error creating identity - %v", err))
		}

		token, err := spectersClient.GetToken(context.Background(), identity)
		if err != nil {
			Fatal(fmt.Errorf("login %v", err))
		}

		fmt.Printf(loginMsg, token)

		viper.Set("token", token)
		err = viper.WriteConfig()
		if err != nil {
			Fatal(fmt.Errorf("error saving config - %v", err))
		}
	},
	Args: cobra.MaximumNArgs(1),
}
