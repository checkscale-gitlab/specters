package cmd

import (
	"context"
	"fmt"
	"github.com/multiformats/go-multiaddr"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/socialspecters.io/specters/pkg/api/client"
	"gitlab.com/socialspecters.io/specters/pkg/util"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"os"
)

var (
	// Used for flags.
	cfgFile        string
	version        string
	spectersClient *client.Client

	rootCmd = &cobra.Command{
		Use:   "specters-cli",
		Short: "specter-cli is a command line client for Social Specters",
		Long:  `A Social Specters client built with love by PL in Go.`,
		PersistentPreRun: func(c *cobra.Command, args []string) {
			apiAddr, err := multiaddr.NewMultiaddr(viper.GetString("api"))
			if err != nil {
				Fatal(err)
			}

			target, err := util.TCPAddrFromMultiAddr(apiAddr)
			if err != nil {
				Fatal(err)
			}

			spectersClient, err = client.NewClient(
				context.Background(),
				target,
				grpc.WithTransportCredentials(insecure.NewCredentials()),
			)
			if err != nil {
				Fatal(err)
			}
		},
		PersistentPostRun: func(c *cobra.Command, args []string) {
			err := spectersClient.Close()
			if err != nil {
				Fatal(err)
			}
		},
		Args: cobra.NoArgs,
	}
)

// Execute executes the root command.
func Execute(v string) error {
	version = v
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/specters.yaml)")
	rootCmd.PersistentFlags().StringP("api", "a", "/ip4/127.0.0.1/tcp/6006", "API target")
	rootCmd.PersistentFlags().Bool("viper", true, "use Viper for configuration")
	viper.SetEnvPrefix("spctrs")
	_ = viper.BindPFlag("api", rootCmd.PersistentFlags().Lookup("api"))
	_ = viper.BindEnv("api")
	_ = viper.BindEnv("token")
	_ = viper.BindPFlag("useViper", rootCmd.PersistentFlags().Lookup("viper"))
	viper.SetDefault("api", "/ip4/127.0.0.1/tcp/6006")
	viper.SetDefault("token", "")
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		viper.SetConfigPermissions(os.FileMode(0o600))

		// Writes first time the config file
		configPath := fmt.Sprintf("%s/.config/specters.yaml", home)
		if err = viper.SafeWriteConfigAs(configPath); err != nil {
			if os.IsNotExist(err) {
				err = viper.WriteConfigAs(configPath)
				cobra.CheckErr(err)
			}
		}

		// Search config in home directory with name ".config/specters" (without extension).
		viper.AddConfigPath(fmt.Sprintf("%s/.config", home))
		viper.SetConfigType("yaml")
		viper.SetConfigName("specters")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

// Fatal prints the error and exit
func Fatal(err error) {
	fmt.Printf(`specters-cli: %s`, err)
	fmt.Println()
	os.Exit(1)
}
