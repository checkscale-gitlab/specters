package util

import (
	"github.com/multiformats/go-multiaddr"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLevelFromDebugFlag(t *testing.T) {
	tests := []struct {
		name      string
		debug     bool
		expecting string
	}{
		{
			name:      "Info logging level",
			debug:     false,
			expecting: "info",
		},
		{
			name:      "Debug logging level",
			debug:     true,
			expecting: "debug",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			assert.Equal(tt, tc.expecting, LevelFromDebugFlag(tc.debug))
		})
	}
}

func TestTCPAddrFromMultiAddr(t *testing.T) {
	tests := []struct {
		name          string
		multiAddr     string
		expectingAddr string
		expectingErr  bool
	}{
		{
			name:          "All success no error",
			multiAddr:     "/ip4/127.0.0.1/tcp/6006",
			expectingAddr: "127.0.0.1:6006",
			expectingErr:  false,
		},
		{
			name:          "Error getting protocol IP4",
			multiAddr:     "/ip6/127.0.0.1/tcp/6006",
			expectingAddr: "",
			expectingErr:  true,
		},
		{
			name:          "Error getting protocol TCP",
			multiAddr:     "/ip4/127.0.0.1/udp/6006",
			expectingAddr: "",
			expectingErr:  true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			apiAddr, err := multiaddr.NewMultiaddr(tc.multiAddr)
			if err != nil {
				tt.Fatalf("Failed creating multi address: %v", err)
			}
			addr, err := TCPAddrFromMultiAddr(apiAddr)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, tc.expectingAddr, addr)
		})
	}
}
