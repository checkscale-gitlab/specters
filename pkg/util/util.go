package util

import (
	"fmt"
	"github.com/multiformats/go-multiaddr"
)

// LevelFromDebugFlag returns the debug or info log level.
func LevelFromDebugFlag(debug bool) string {
	if debug {
		return "debug"
	}
	return "info"
}

// TCPAddrFromMultiAddr transforms multi address in a TCP address
func TCPAddrFromMultiAddr(m multiaddr.Multiaddr) (addr string, err error) {
	ip4, err := m.ValueForProtocol(multiaddr.P_IP4)
	if err != nil {
		return
	}
	tcp, err := m.ValueForProtocol(multiaddr.P_TCP)
	if err != nil {
		return
	}
	return fmt.Sprintf("%s:%s", ip4, tcp), nil
}
