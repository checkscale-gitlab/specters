package specters

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/golang/mock/gomock"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	mock_db "gitlab.com/socialspecters.io/specters/internal/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"io"
	"testing"
)

func TestNewSpecters(t *testing.T) {
	tests := []struct {
		name               string
		debug              bool
		db                 func(t *testing.T) *mock_db.MockDB
		loggingSetLogLevel func(t *testing.T) func(name, level string) error
		expectingErr       bool
	}{
		{
			name:  "NewSpecters with info logging",
			debug: false,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					t.Helper()
					assert.Equal(t, "specters", name)
					assert.Equal(t, "info", level)

					return nil
				}
			},
			expectingErr: false,
		},
		{
			name:  "NewSpecters with debug logging",
			debug: true,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					t.Helper()
					assert.Equal(t, "specters", name)
					assert.Equal(t, "debug", level)

					return nil
				}
			},
			expectingErr: false,
		},
		{
			name:  "Error creating NewSpecters",
			debug: false,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				return mock
			},
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					return fmt.Errorf("")
				}
			},
			expectingErr: true,
		},
		{
			name:  "Error creating specters DB",
			debug: true,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(
					fmt.Errorf(""),
				)
				return mock
			},
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					return nil
				}
			},
			expectingErr: true,
		},
		{
			name:  "Error checking if has specters",
			debug: true,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, fmt.Errorf(""))
				return mock
			},
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					return nil
				}
			},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origLoggingSetLogLevel := loggingSetLogLevel
			loggingSetLogLevel = tc.loggingSetLogLevel(tt)
			defer func() { loggingSetLogLevel = origLoggingSetLogLevel }()

			s, err := NewSpecters(context.Background(), "cluster-id", "version", tc.db(tt), Config{Debug: tc.debug})
			errExist := err != nil

			if err == nil {
				assert.True(tt, s.IsInitialized())
				assert.True(tt, s.IsSealed())
				assert.Equal(tt, "cluster-id", s.ClusterID())
				assert.Equal(tt, "version", s.Version())
			}

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestSpecters_Init(t *testing.T) {
	_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error creating root key")
	}
	pk := NewLibP2PPubKey(pubKey)
	pkBinary, _ := pk.MarshalBinary()

	tests := []struct {
		name                     string
		pk                       []byte
		db                       func(t *testing.T) *mock_db.MockDB
		cryptoGenerateEd25519Key func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error)
		expectingErr             bool
	}{
		{
			name: "All success no error",
			pk:   pkBinary,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Do(func(arg0 *SpecterDB, arg1 interface{}) (db.InstanceID, error) {
					assert.Equal(t, arg0.PubKey, EncodeKey(pkBinary))
					assert.Equal(t, arg0.Alias, "root")
					assert.NotNil(t, arg0.CreatedAt)
					assert.Equal(t, arg0.pubKey, pk)
					assert.NotNil(t, arg1)
					return "", nil
				})
				return mock
			},
			cryptoGenerateEd25519Key: func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
				return func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
					return crypto.GenerateEd25519Key(src)
				}
			},
			expectingErr: false,
		},
		{
			name: "Error already initialized",
			pk:   pkBinary,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			cryptoGenerateEd25519Key: func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
				return func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
					return crypto.GenerateEd25519Key(src)
				}
			},
			expectingErr: true,
		},
		{
			name: "Error generating key",
			pk:   pkBinary,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				return mock
			},
			cryptoGenerateEd25519Key: func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
				return func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
					return nil, nil, fmt.Errorf("")
				}
			},
			expectingErr: true,
		},
		{
			name: "Error creating root specter",
			pk:   []byte{},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				return mock
			},
			cryptoGenerateEd25519Key: func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
				return func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
					return crypto.GenerateEd25519Key(src)
				}
			},
			expectingErr: true,
		},
		{
			name: "Error saving root specter",
			pk:   pkBinary,
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), fmt.Errorf(""))
				return mock
			},
			cryptoGenerateEd25519Key: func(t *testing.T) func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
				return func(src io.Reader) (crypto.PrivKey, crypto.PubKey, error) {
					return crypto.GenerateEd25519Key(src)
				}
			},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origCryptoGenerateEd25519Key := cryptoGenerateEd25519Key
			cryptoGenerateEd25519Key = tc.cryptoGenerateEd25519Key(tt)
			defer func() { cryptoGenerateEd25519Key = origCryptoGenerateEd25519Key }()

			s, _ := NewSpecters(context.Background(), "", "", tc.db(tt), Config{Debug: false})
			parts, err := s.Init(tc.pk)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			if !errExist {
				assert.True(tt, s.initialized)
				assert.Equal(tt, 0, s.UnsealProgress())
				assert.Equal(tt, ShamirParts, len(parts))
			}
		})
	}
}

func TestSpecters_Unseal(t *testing.T) {
	tests := []struct {
		name            string
		db              func(t *testing.T) *mock_db.MockDB
		parts           []int
		initialized     bool
		unsealed        bool
		changeOneByte   bool
		expectingErr    bool
		expectingEndErr bool
	}{
		{
			name: "All success no error",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			parts:           []int{0, 1, 2},
			initialized:     true,
			unsealed:        false,
			changeOneByte:   false,
			expectingErr:    false,
			expectingEndErr: false,
		},
		{
			name: "Error not initialized",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				return mock
			},
			parts:           []int{0, 1, 2},
			initialized:     false,
			unsealed:        false,
			changeOneByte:   false,
			expectingErr:    true,
			expectingEndErr: false,
		},
		{
			name: "Error already unsealed",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			parts:           []int{0, 1, 2},
			initialized:     true,
			unsealed:        true,
			changeOneByte:   false,
			expectingErr:    true,
			expectingEndErr: false,
		},
		{
			name: "Error combining parts",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			parts:           []int{0, 1, 1},
			initialized:     true,
			unsealed:        false,
			changeOneByte:   false,
			expectingErr:    false,
			expectingEndErr: true,
		},
		{
			name: "Error unmarshalling identity",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			parts:           []int{0, 1, 2},
			initialized:     true,
			unsealed:        false,
			expectingErr:    false,
			changeOneByte:   true,
			expectingEndErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			s, _ := NewSpecters(context.Background(), "", "", tc.db(tt), Config{Debug: false})
			var parts [][]byte
			if tc.initialized {
				_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
				if err != nil {
					tt.Fatalf("Error creating root key")
				}
				pk, _ := NewLibP2PPubKey(pubKey).MarshalBinary()
				parts, _ = s.Init(pk)
			} else {
				parts = [][]byte{{0}}
			}
			if tc.unsealed {
				s.SetIdentity(&LibP2PIdentity{})
			}
			err := s.Unseal(parts[tc.parts[0]])
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)

			if !errExist {
				assert.Equal(tt, 1, s.UnsealProgress())
				_ = s.Unseal(parts[tc.parts[1]])
				assert.Equal(tt, 2, s.UnsealProgress())

				if tc.changeOneByte {
					parts[tc.parts[2]][0] = 0
				}

				err = s.Unseal(parts[tc.parts[2]])
				errExist = err != nil

				assert.Equal(tt, tc.expectingEndErr, errExist)
				if errExist {
					assert.Equal(tt, 0, len(s.shamir))
				} else {
					assert.Equal(tt, 3, s.UnsealProgress())
					assert.False(tt, s.IsSealed())
				}
			}
		})
	}
}

func TestSpecters_GetSpecterByPubKey(t *testing.T) {
	tests := []struct {
		name         string
		pk           string
		db           func(t *testing.T) *mock_db.MockDB
		expectingErr bool
	}{
		{
			name: "All success no error",
			pk:   "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &SpecterDB{}, gomock.Any()).Return(&SpecterDB{
					PubKey: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
				}, nil)
				return mock
			},
			expectingErr: false,
		},
		{
			name: "Error getting specter from DB",
			pk:   "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &SpecterDB{}, gomock.Any()).Return(
					nil, fmt.Errorf(""),
				)
				return mock
			},
			expectingErr: true,
		},
		{
			name: "Error Specter not found",
			pk:   "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &SpecterDB{}, gomock.Any()).Return(
					nil, nil,
				)
				return mock
			},
			expectingErr: false,
		},
		{
			name: "Error init Specters",
			pk:   "****",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("****", &SpecterDB{}, gomock.Any()).Return(&SpecterDB{
					PubKey: "****",
				}, nil)
				return mock
			},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			key, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key")
			}

			s, err := NewSpecters(context.Background(), "", "", tc.db(tt), Config{Debug: false})
			s.SetIdentity(NewLibP2PIdentity(key))
			if err != nil {
				tt.Fatal("Error creating Specters manager")
			}
			_, err = s.GetSpecterByPubKey(tc.pk)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestSpecters_HasSpecters(t *testing.T) {
	tests := []struct {
		name                 string
		db                   func(t *testing.T) *mock_db.MockDB
		expectingHasSpecters bool
		expectingErr         bool
	}{
		{
			name: "All success no error",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil).Times(2)
				return mock
			},
			expectingHasSpecters: true,
			expectingErr:         false,
		},
		{
			name: "Has not specters",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil).Times(2)
				return mock
			},
			expectingHasSpecters: false,
			expectingErr:         false,
		},
		{
			name: "Error checking if has specters",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().HasSpecters().Return(false, fmt.Errorf(""))
				return mock
			},
			expectingHasSpecters: false,
			expectingErr:         true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			key, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key")
			}

			s, err := NewSpecters(context.Background(), "", "", tc.db(tt), Config{Debug: false})
			s.SetIdentity(NewLibP2PIdentity(key))
			if err != nil {
				tt.Fatal("Error creating Specters manager")
			}
			hasSpecters, err := s.HasSpecters()
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			assert.Equal(tt, tc.expectingHasSpecters, hasSpecters)
		})
	}
}

func TestSpecters_GetToken(t *testing.T) {
	tests := []struct {
		name         string
		pk           string
		db           func(t *testing.T) *mock_db.MockDB
		expectingErr bool
	}{
		{
			name: "All success no error",
			pk:   "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			specter, err := NewSpecterDB(tc.pk)
			if err != nil {
				tt.Fatalf("Error creating specter")
			}

			key, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key")
			}

			s, err := NewSpecters(context.Background(), "", "", tc.db(tt), Config{Debug: false})
			if err != nil {
				tt.Fatal("Error creating Specters manager")
			}
			s.SetIdentity(NewLibP2PIdentity(key))
			tokenStr, err := s.GetToken(specter)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)

			if !errExist {
				pubKeyRaw, _ := key.GetPublic().Raw()
				token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodEd25519); !ok {
						return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
					}

					return ed25519.PublicKey(pubKeyRaw), nil
				})
				if err != nil {
					t.Fatalf("Error parsing token: %v", err)
				}

				if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
					assert.Equal(t, tc.pk, claims["sub"])
					assert.Equal(t, EncodeKey(pubKeyRaw), claims["iss"])
					assert.NotNil(t, claims["iat"])
				} else {
					t.Fatalf("Error getting JWT claims: %v", err)
				}
			}
		})
	}
}
