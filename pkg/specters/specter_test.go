package specters

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"testing"
)

func TestNewSpecterDB(t *testing.T) {
	tests := []struct {
		name         string
		pk           string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			pk:           "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			specter, err := NewSpecterDB(tc.pk, WithAlias("root"))
			errExist := err != nil
			specter.SetID("an-id")
			specterDB, _ := specter.(*SpecterDB)

			assert.Equal(tt, tc.expectingErr, errExist)
			assert.NotNil(tt, specter)
			assert.Equal(tt, specter.GetID(), db.InstanceID("an-id"))
			assert.Equal(tt, specterDB.PubKey, tc.pk)
			assert.Equal(tt, specterDB.Alias, "root")
			assert.NotNil(tt, specterDB.CreatedAt)
			assert.Equal(tt, specterDB.Signature, "")
		})
	}
}

func TestSpecterDB_Init(t *testing.T) {
	tests := []struct {
		name         string
		pk           string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			pk:           "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			expectingErr: false,
		},
		{
			name:         "Error decoding key",
			pk:           "****",
			expectingErr: true,
		},
		{
			name:         "Error unmarshalling key",
			pk:           "CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=",
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			specter := &SpecterDB{
				PubKey: tc.pk,
			}
			err := specter.Init()
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}
func TestSpecterDB_PubKeyString(t *testing.T) {
	tests := []struct {
		name         string
		pubKey       string
		expectingErr bool
	}{
		{
			name:   "All success no error",
			pubKey: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			specter, err := NewSpecterDB(tc.pubKey)
			if err != nil {
				t.Fatalf("Error creating SpecterDB: %v", err)
			}

			assert.Equal(tt, tc.pubKey, specter.GetPubKey())
		})
	}
}

func TestSpecterDB_MarshalBinary(t *testing.T) {
	specter, err := NewSpecterDB("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C")
	if err != nil {
		t.Fatalf("Error creating SpecterDB: %v", err)
	}

	tests := []struct {
		name         string
		specter      Specter
		expectingErr bool
	}{
		{
			name:         "All success no error",
			specter:      specter,
			expectingErr: false,
		},
		{
			name:         "Error not initialized",
			specter:      &SpecterDB{},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			bytes, err := tc.specter.MarshalBinary()
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			if errExist {
				assert.Nil(tt, bytes)
			} else {
				assert.NotNil(tt, bytes)
			}
		})
	}
}

func TestSpecterDB_Raw(t *testing.T) {
	key, _, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating server key")
	}

	identity := NewLibP2PIdentity(key)
	pubKeyRaw, _ := identity.GetPublic().MarshalBinary()

	specter, err := NewSpecterDB(EncodeKey(pubKeyRaw))
	if err != nil {
		t.Fatalf("Error creating Specter")
	}

	tests := []struct {
		name         string
		specter      Specter
		expectingErr bool
	}{
		{
			name:         "All success no error",
			specter:      specter,
			expectingErr: false,
		},
		{
			name:         "Error not initialized",
			specter:      &SpecterDB{},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			identityRaw, err := identity.GetPublic().Raw()
			if err != nil {
				t.Fatalf("Error getting Identity raw")
			}
			specterRaw, err := tc.specter.Raw()
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			if errExist {
				assert.Nil(tt, specterRaw)
			} else {
				assert.Equal(tt, identityRaw, specterRaw)
			}
		})
	}
}

func TestSpecter_Verify(t *testing.T) {
	key, _, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating server key")
	}

	identity := NewLibP2PIdentity(key)
	pubKeyRaw, _ := identity.GetPublic().MarshalBinary()

	specter, err := NewSpecterDB(EncodeKey(pubKeyRaw))
	if err != nil {
		t.Fatalf("Error creating Specter")
	}

	tests := []struct {
		name           string
		specter        Specter
		expectingValid bool
		expectingErr   bool
	}{
		{
			name:           "All success no error",
			specter:        specter,
			expectingValid: true,
			expectingErr:   false,
		},
		{
			name:           "Error specter not init",
			specter:        &SpecterDB{},
			expectingValid: false,
			expectingErr:   true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			signedData, err := key.Sign([]byte("data-to-sign"))
			if err != nil {
				tt.Fatal("Error signing data")
			}

			valid, err := tc.specter.Verify([]byte("data-to-sign"), signedData)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			assert.Equal(tt, tc.expectingValid, valid)
		})
	}
}

func TestSpecterDB_SignInstance(t *testing.T) {
	key, _, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating server key")
	}

	identity := NewLibP2PIdentity(key)
	pubKeyRaw, _ := identity.GetPublic().MarshalBinary()

	specter, err := NewSpecterDB(EncodeKey(pubKeyRaw), WithAlias("alias"))
	if err != nil {
		t.Fatalf("Error creating Specter")
	}
	specter.SetID("an-id")

	tests := []struct {
		name           string
		jsonMarshal    func(t *testing.T) func(v interface{}) ([]byte, error)
		expectingValid bool
		expectingErr   bool
	}{
		{
			name: "All success no error",
			jsonMarshal: func(t *testing.T) func(v interface{}) ([]byte, error) {
				return func(v interface{}) ([]byte, error) {
					return json.Marshal(v)
				}
			},
			expectingValid: true,
			expectingErr:   false,
		},
		{
			name: "Error marshalling specter",
			jsonMarshal: func(t *testing.T) func(v interface{}) ([]byte, error) {
				return func(v interface{}) ([]byte, error) {
					return nil, fmt.Errorf("")
				}
			},
			expectingValid: false,
			expectingErr:   true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origJSONMarshall := jsonMarshal
			jsonMarshal = tc.jsonMarshal(tt)
			defer func() { jsonMarshal = origJSONMarshall }()

			err = specter.SignInstance(identity)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			if errExist {
				assert.Equal(tt, "", specter.(*SpecterDB).Signature)
			} else {
				assert.NotNil(tt, specter.(*SpecterDB).Signature)
			}
		})
	}
}

func TestSpecterDB_VerifyInstance(t *testing.T) {
	key, _, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating server key")
	}

	identity := NewLibP2PIdentity(key)
	pubKeyRaw, _ := identity.GetPublic().MarshalBinary()

	specter, err := NewSpecterDB(EncodeKey(pubKeyRaw), WithAlias("alias"))
	if err != nil {
		t.Fatalf("Error creating Specter")
	}
	specter.SetID("an-id")

	tests := []struct {
		name           string
		jsonMarshal    func(t *testing.T) func(v interface{}) ([]byte, error)
		expectingValid bool
		expectingErr   bool
	}{
		{
			name: "All success no error",
			jsonMarshal: func(t *testing.T) func(v interface{}) ([]byte, error) {
				return func(v interface{}) ([]byte, error) {
					return json.Marshal(v)
				}
			},
			expectingValid: true,
			expectingErr:   false,
		},
		{
			name: "Error marshalling specter",
			jsonMarshal: func(t *testing.T) func(v interface{}) ([]byte, error) {
				return func(v interface{}) ([]byte, error) {
					return nil, fmt.Errorf("")
				}
			},
			expectingValid: false,
			expectingErr:   true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			err = specter.SignInstance(identity)
			if err != nil {
				tt.Fatal("Error signing data")
			}
			specterDB := specter.(*SpecterDB)

			origJSONMarshall := jsonMarshal
			jsonMarshal = tc.jsonMarshal(tt)
			defer func() { jsonMarshal = origJSONMarshall }()

			specterDB.pubKey = nil
			valid, err := specter.VerifyInstance(identity.GetPublicDB())
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
			assert.Equal(tt, tc.expectingValid, valid)
			assert.NotNil(tt, specterDB.Signature)
		})
	}
}
