package specters

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"fmt"
	"github.com/golang-jwt/jwt"
	logging "github.com/ipfs/go-log/v2"
	"github.com/libp2p/go-libp2p-core/crypto"
	"gitlab.com/socialspecters.io/specters/internal/pkg/shamir"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/util"
	"time"
)

// ShamirParts is the number of parts generating by shamir algorithm
const ShamirParts = 5

// ShamirThreshold is the necessary number of parts for unsealing process
const ShamirThreshold = 3

var (
	loggingSetLogLevel       = logging.SetLogLevel
	log                      = logging.Logger("specters")
	cryptoGenerateEd25519Key = crypto.GenerateEd25519Key
)

// Specters is an app for specters orchestration
type Specters struct {
	clusterID string
	version   string
	db        db.DB
	identity  Identity

	initialized bool
	shamir      [][]byte
}

// NewSpecters creates a new instance of Specters
func NewSpecters(ctx context.Context, clusterID string, version string, db db.DB, config Config) (*Specters, error) {
	err := loggingSetLogLevel("specters", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	err = db.CreateSpecters(ctx, &SpecterDB{})
	if err != nil {
		return nil, err
	}

	// If exists at least root specter, it has been initialized
	initialized, err := db.HasSpecters()
	if err != nil {
		return nil, err
	}

	return &Specters{
		clusterID:   clusterID,
		version:     version,
		db:          db,
		initialized: initialized,
		shamir:      make([][]byte, 0),
	}, nil
}

// SetIdentity set the identity for the Specters manager
func (s *Specters) SetIdentity(identity Identity) {
	s.initialized = identity != nil
	s.identity = identity
}

// ClusterID returns the Specters clusterID
func (s *Specters) ClusterID() string {
	return s.clusterID
}

// Version returns the Specters version
func (s *Specters) Version() string {
	return s.version
}

// IsInitialized returns if Specters is already initialized
func (s *Specters) IsInitialized() bool {
	return s.initialized
}

// Init generates a master identity split in several parts
func (s *Specters) Init(pk []byte) ([][]byte, error) {
	log.Debug("initializing specters")

	if s.initialized {
		return nil, fmt.Errorf("specters already initialized")
	}

	// Generating master key
	key, _, err := cryptoGenerateEd25519Key(rand.Reader)
	if err != nil {
		return nil, err
	}
	identity := NewLibP2PIdentity(key)

	// Creating Root Specter
	root, err := NewSpecterDB(EncodeKey(pk), WithAlias("root"))
	if err != nil {
		return nil, err
	}

	_, err = s.db.CreateSpecter(root, identity)
	if err != nil {
		return nil, err
	}

	// MarshalBinary can not fail
	secret, _ := identity.MarshalBinary()
	s.initialized = true
	s.shamir = make([][]byte, 0)
	return shamir.Split(secret, ShamirParts, ShamirThreshold)
}

// Unseal set a part to unseal the master key and set it if the enough parts are set
func (s *Specters) Unseal(part []byte) error {
	log.Debug("unsealing specters")

	if !s.initialized {
		return fmt.Errorf("specters not initialized")
	}

	if !s.IsSealed() {
		return fmt.Errorf("specters already unsealed")
	}

	s.shamir = append(s.shamir, part)

	if len(s.shamir) == ShamirThreshold {
		secret, err := shamir.Combine(s.shamir)
		if err != nil {
			s.shamir = make([][]byte, 0)
			return err
		}

		s.identity, err = UnmarshalLibP2PIdentity(secret)
		if err != nil {
			s.shamir = make([][]byte, 0)
			return err
		}
	}

	return nil
}

// UnsealProgress gets the number of parts provided to unseal the Specters manager
func (s *Specters) UnsealProgress() int {
	if !s.IsSealed() {
		return ShamirThreshold
	}
	return len(s.shamir)
}

// IsSealed gets if the Specters' manager is sealed
func (s *Specters) IsSealed() bool {
	return s.identity == nil
}

// GetSpecterByPubKey returns a registered Specter identity from a public key
func (s *Specters) GetSpecterByPubKey(pk string) (specter Specter, err error) {
	log.Debug("getting specter by public key")

	dbSpecter, err := s.db.GetSpecter(pk, &SpecterDB{}, s.identity.GetPublicDB())
	if err != nil {
		return
	}
	if dbSpecter == nil {
		return
	}

	specter, _ = dbSpecter.(Specter)
	err = specter.Init()
	if err != nil {
		return
	}

	return
}

// HasSpecters returns if at least one specter exists
func (s *Specters) HasSpecters() (bool, error) {
	log.Debug("checking if has specters")
	return s.db.HasSpecters()
}

// GetToken returns a token for a valid Specter
func (s *Specters) GetToken(specter Specter) (string, error) {
	log.Debug("getting JW token for Specter")

	// Raw public key bytes of an Ed25519PublicKey can not fail
	pubKeyRaw, _ := s.identity.GetPublic().Raw()
	token := jwt.NewWithClaims(jwt.SigningMethodEdDSA, jwt.StandardClaims{
		Subject:  specter.GetPubKey(),
		Issuer:   EncodeKey(pubKeyRaw),
		IssuedAt: time.Now().Unix(),
	})

	// Raw private key bytes of an Ed25519PrivateKey can not fail
	keyRaw, _ := s.identity.Raw()
	return token.SignedString(ed25519.PrivateKey(keyRaw))
}
