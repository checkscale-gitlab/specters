package server

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/golang/mock/gomock"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	mockpb "gitlab.com/socialspecters.io/specters/internal/pkg/api/pb"
	mock_db "gitlab.com/socialspecters.io/specters/internal/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
	"time"
)

func TestNewServer(t *testing.T) {
	tests := []struct {
		name               string
		debug              bool
		loggingSetLogLevel func(t *testing.T) func(name, level string) error
		expectingErr       bool
	}{
		{
			name:  "NewServer with info logging",
			debug: false,
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					t.Helper()
					assert.Equal(t, "specters:api", name)
					assert.Equal(t, "info", level)

					return nil
				}
			},
			expectingErr: false,
		},
		{
			name:  "NewServer with debug logging",
			debug: true,
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					t.Helper()
					assert.Equal(t, "specters:api", name)
					assert.Equal(t, "debug", level)

					return nil
				}
			},
			expectingErr: false,
		},
		{
			name:  "Error creating NewServer",
			debug: false,
			loggingSetLogLevel: func(t *testing.T) func(name string, level string) error {
				return func(name string, level string) error {
					return fmt.Errorf("")
				}
			},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origLoggingSetLogLevel := loggingSetLogLevel
			loggingSetLogLevel = tc.loggingSetLogLevel(t)
			defer func() { loggingSetLogLevel = origLoggingSetLogLevel }()

			key, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key")
			}

			mock := mock_db.NewMockDB(gomock.NewController(tt))
			mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
			mock.EXPECT().HasSpecters().Return(true, nil)
			manager, err := specters.NewSpecters(context.Background(), "", "", mock, specters.Config{Debug: tc.debug})
			manager.SetIdentity(specters.NewLibP2PIdentity(key))
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}
			_, err = NewServer(manager, Config{Debug: tc.debug})
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestServer_Status(t *testing.T) {
	tests := []struct {
		name         string
		db           func(t *testing.T) *mock_db.MockDB
		clusterID    string
		version      string
		expectingErr bool
	}{
		{
			name: "All success no error",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			clusterID:    "cluster-id",
			version:      "version",
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			manager, err := specters.NewSpecters(context.Background(), tc.clusterID, tc.version, tc.db(tt), specters.Config{Debug: false})
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}

			s, _ := NewServer(manager, Config{Debug: false})
			reply, err := s.Status(context.Background(), &pb.StatusRequest{})
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, tc.clusterID, reply.ClusterID)
			assert.Equal(tt, tc.version, reply.Version)
			assert.True(tt, reply.Initialized)
			assert.True(tt, reply.Sealed)
			assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
		})
	}
}

func TestServer_Init(t *testing.T) {
	tests := []struct {
		name         string
		db           func(t *testing.T) *mock_db.MockDB
		expectingErr bool
	}{
		{
			name: "All success no error",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			expectingErr: false,
		},
		{
			name: "Error init specters",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			manager, err := specters.NewSpecters(context.Background(), "", "", tc.db(tt), specters.Config{Debug: false})
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}

			_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				t.Fatalf("Error creating root key")
			}
			pk, _ := specters.NewLibP2PPubKey(pubKey).MarshalBinary()
			s, _ := NewServer(manager, Config{Debug: false})
			reply, err := s.Init(context.Background(), &pb.InitRequest{
				RootPublicKey: pk,
			})
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)

			if !errExist {
				assert.Equal(tt, specters.ShamirParts, len(reply.Keys))
				assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
			}
		})
	}
}

func TestServer_Unseal(t *testing.T) {
	tests := []struct {
		name         string
		db           func(t *testing.T) *mock_db.MockDB
		unsealed     bool
		expectingErr bool
	}{
		{
			name: "All success no error",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			unsealed:     false,
			expectingErr: false,
		},
		{
			name: "Error unsealing specters",
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				mock.EXPECT().CreateSpecter(gomock.Any(), gomock.Any()).Return(db.InstanceID(""), nil)
				return mock
			},
			unsealed:     true,
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			manager, err := specters.NewSpecters(context.Background(), "", "", tc.db(tt), specters.Config{Debug: false})
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}

			_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				t.Fatalf("Error creating root key")
			}
			pk, _ := specters.NewLibP2PPubKey(pubKey).MarshalBinary()
			keys, _ := manager.Init(pk)

			if tc.unsealed {
				manager.SetIdentity(&specters.LibP2PIdentity{})
			}

			s, _ := NewServer(manager, Config{Debug: false})
			reply, err := s.Unseal(context.Background(), &pb.UnsealRequest{
				Key: keys[0],
			})
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)

			if !errExist {
				assert.Equal(tt, "", reply.ClusterID)
				assert.Equal(tt, "", reply.Version)
				assert.True(tt, reply.Initialized)
				assert.True(tt, reply.Sealed)
				assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
				assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
				assert.Equal(tt, int32(1), reply.Progress)
			}
		})
	}
}

func TestServer_GetToken(t *testing.T) {
	challenge := []byte("challenge")

	defaultRandRead := func(t *testing.T) func(b []byte) (n int, err error) {
		return func(b []byte) (n int, err error) {
			for i, n := range challenge {
				b[i] = n
			}
			return 0, nil
		}
	}

	decodedIdentity, err := specters.DecodeKey("CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=")
	if err != nil {
		t.Fatal("Error decoding identity")
	}

	identity, err := specters.UnmarshalLibP2PIdentity(decodedIdentity)
	if err != nil {
		t.Fatal("Error unmarshalling identity")
	}

	specter, err := specters.NewSpecterDB("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C")
	if err != nil {
		t.Fatalf("Error creating Specter: %v", err)
	}

	signature, err := identity.Sign(challenge)
	if err != nil {
		t.Fatal("Error signing challenge")
	}

	origKey, _, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating server key: %v", err)
	}

	tests := []struct {
		name                   string
		client                 func(t *testing.T) *mockpb.MockAPI_GetTokenServer
		db                     func(t *testing.T) *mock_db.MockDB
		tokenChallengeTimeout  time.Duration
		forceUseOtherTypeOfKey bool
		randRead               func(t *testing.T) func(b []byte) (n int, err error)
		expectingErr           bool
		expectingErrCode       codes.Code
	}{
		{
			name: "All success no error",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Signature{
						Signature: signature,
					},
				}, nil)
				mock.EXPECT().Send(gomock.Any()).Do(func(arg0 *pb.GetTokenReply) {
					pubKeyRaw, _ := origKey.GetPublic().Raw()
					token, err := jwt.Parse(arg0.GetToken(), func(token *jwt.Token) (interface{}, error) {
						if _, ok := token.Method.(*jwt.SigningMethodEd25519); !ok {
							return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
						}

						return ed25519.PublicKey(pubKeyRaw), nil
					})
					if err != nil {
						t.Fatalf("Error parsing token: %v", err)
					}

					if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
						assert.Equal(t, "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", claims["sub"])
						assert.Equal(t, specters.EncodeKey(pubKeyRaw), claims["iss"])
						assert.NotNil(t, claims["iat"])
					} else {
						t.Fatalf("Error getting JWT claims: %v", err)
					}
				})

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()

				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)

				return mock
			},
			randRead:     defaultRandRead,
			expectingErr: false,
		},
		{
			name: "Error receiving key",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(nil, fmt.Errorf(""))

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			randRead:     defaultRandRead,
			expectingErr: true,
		},
		{
			name: "Error receiving empty key",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			randRead:         defaultRandRead,
			expectingErr:     true,
			expectingErrCode: codes.InvalidArgument,
		},
		{
			name: "Error authenticating specter",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "****",
					},
				}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter(gomock.Any(), &specters.SpecterDB{}, gomock.Any()).Return(nil, fmt.Errorf(""))
				return mock
			},
			randRead:         defaultRandRead,
			expectingErr:     true,
			expectingErrCode: codes.Unauthenticated,
		},
		{
			name: "Error not found specter",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter(gomock.Any(), &specters.SpecterDB{}, gomock.Any()).Return(
					nil, nil,
				)
				return mock
			},
			randRead:         defaultRandRead,
			expectingErr:     true,
			expectingErrCode: codes.Unauthenticated,
		},
		{
			name: "Error creating challenge",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead: func(t *testing.T) func(b []byte) (n int, err error) {
				return func(b []byte) (n int, err error) {
					return 0, fmt.Errorf("")
				}
			},
			expectingErr: true,
		},
		{
			name: "Error sending challenge",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(gomock.Any()).Return(fmt.Errorf(""))

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead:     defaultRandRead,
			expectingErr: true,
		},
		{
			name: "Error receiving empty signature timeout",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().AnyTimes().Return(nil, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			tokenChallengeTimeout: time.Nanosecond,
			randRead:              defaultRandRead,
			expectingErr:          true,
			expectingErrCode:      codes.DeadlineExceeded,
		},
		{
			name: "Error receiving signature",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(nil, fmt.Errorf(""))

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead:     defaultRandRead,
			expectingErr: true,
		},
		{
			name: "Error receiving empty signature",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead:         defaultRandRead,
			expectingErr:     true,
			expectingErrCode: codes.InvalidArgument,
		},
		{
			name: "Error validating signature",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Signature{
						Signature: []byte{},
					},
				}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead:         defaultRandRead,
			expectingErr:     true,
			expectingErrCode: codes.InvalidArgument,
		},
		{
			name: "Error generating token",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Signature{
						Signature: signature,
					},
				}, nil)

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			forceUseOtherTypeOfKey: true,
			randRead:               defaultRandRead,
			expectingErr:           true,
		},
		{
			name: "Error sending token",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()

				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				}, nil)
				mock.EXPECT().Context().Return(context.TODO())
				mock.EXPECT().Send(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: challenge,
					},
				})
				mock.EXPECT().Recv().Return(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Signature{
						Signature: signature,
					},
				}, nil)
				mock.EXPECT().Send(gomock.Any()).Return(fmt.Errorf(""))

				return mock
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				mock.EXPECT().GetSpecter("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", &specters.SpecterDB{}, gomock.Any()).Return(specter, nil)
				return mock
			},
			randRead:     defaultRandRead,
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origTokenChallengeBytes := tokenChallengeBytes
			tokenChallengeBytes = len(challenge)
			defer func() { tokenChallengeBytes = origTokenChallengeBytes }()

			origRandRead := randRead
			randRead = tc.randRead(t)
			defer func() { randRead = origRandRead }()

			if tc.tokenChallengeTimeout > 0 {
				origTokenChallengeTimeout := tokenChallengeTimeout
				tokenChallengeTimeout = tc.tokenChallengeTimeout
				defer func() { tokenChallengeTimeout = origTokenChallengeTimeout }()
			}

			var key crypto.PrivKey
			if tc.forceUseOtherTypeOfKey {
				key, _, err = crypto.GenerateRSAKeyPair(2048, rand.Reader)
				if err != nil {
					tt.Fatalf("Error generating server key: %v", err)
				}
			} else {
				key = origKey
			}

			manager, err := specters.NewSpecters(context.Background(), "", "", tc.db(tt), specters.Config{Debug: false})
			manager.SetIdentity(specters.NewLibP2PIdentity(key))
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}
			s, _ := NewServer(manager, Config{Debug: false})
			err = s.GetToken(tc.client(tt))
			errExist := err != nil

			time.Sleep(tc.tokenChallengeTimeout)

			assert.Equal(tt, tc.expectingErr, errExist, err)
			if tc.expectingErr && tc.expectingErrCode != codes.OK {
				errStatus, _ := status.FromError(err)
				assert.Equal(tt, tc.expectingErrCode, errStatus.Code())
			}
		})
	}
}
