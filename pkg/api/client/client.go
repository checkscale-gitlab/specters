package client

import (
	"context"
	"fmt"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc"
	"io"
)

// Client provides the client api.
type Client struct {
	c    pb.APIClient
	conn *grpc.ClientConn
}

// NewClient starts the client.
func NewClient(ctx context.Context, target string, opts ...grpc.DialOption) (*Client, error) {
	conn, err := grpc.DialContext(ctx, target, opts...)
	if err != nil {
		return nil, err
	}
	return &Client{
		c:    pb.NewAPIClient(conn),
		conn: conn,
	}, nil
}

// Close closes the client's grpc connection and cancels any active requests.
func (c *Client) Close() error {
	return c.conn.Close()
}

// Status gets the status of API server
func (c *Client) Status(ctx context.Context) (*pb.StatusReply, error) {
	return c.c.Status(ctx, &pb.StatusRequest{})
}

// Init creates the master key to unseal Specters
func (c *Client) Init(ctx context.Context, pk []byte) (*pb.InitReply, error) {
	return c.c.Init(ctx, &pb.InitRequest{
		RootPublicKey: pk,
	})
}

// Unseal set a key to unsealing the master key
func (c *Client) Unseal(ctx context.Context, key []byte) (*pb.UnsealReply, error) {
	return c.c.Unseal(ctx, &pb.UnsealRequest{Key: key})
}

// GetToken gets a token for use with the rest of the API.
func (c *Client) GetToken(ctx context.Context, identity specters.Identity) (token string, err error) {
	stream, err := c.c.GetToken(ctx)
	if err != nil {
		return
	}
	defer func() {
		if e := stream.CloseSend(); e != nil && err == nil {
			err = e
		}
	}()

	// Raw public key bytes of an Ed25519PublicKey can not fail
	bytesPubKey, _ := identity.GetPublic().MarshalBinary()
	if err = stream.Send(&pb.GetTokenRequest{
		Payload: &pb.GetTokenRequest_Key{
			Key: specters.EncodeKey(bytesPubKey),
		},
	}); err == io.EOF {
		var noOp interface{}
		return token, stream.RecvMsg(noOp)
	} else if err != nil {
		return
	}

	var challenge []byte
	rep, err := stream.Recv()
	if err != nil {
		return
	}
	switch payload := rep.Payload.(type) {
	case *pb.GetTokenReply_Challenge:
		challenge = payload.Challenge
	default:
		return token, fmt.Errorf("challenge was not received")
	}

	// Sign with Ed25519PublicKey can not fail
	signature, _ := identity.Sign(challenge)
	if err = stream.Send(&pb.GetTokenRequest{
		Payload: &pb.GetTokenRequest_Signature{
			Signature: signature,
		},
	}); err == io.EOF {
		var noOp interface{}
		return token, stream.RecvMsg(noOp)
	} else if err != nil {
		return
	}

	rep, err = stream.Recv()
	if err != nil {
		return
	}
	switch payload := rep.Payload.(type) {
	case *pb.GetTokenReply_Token:
		token = payload.Token
	default:
		return token, fmt.Errorf("token was not received")
	}

	return token, nil
}
