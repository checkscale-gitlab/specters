package main

import (
	"context"
	"errors"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	logging "github.com/ipfs/go-log/v2"
	connmgr "github.com/libp2p/go-libp2p-connmgr"
	"github.com/multiformats/go-multiaddr"
	"github.com/namsral/flag"
	mongods "github.com/textileio/go-ds-mongo"
	"github.com/textileio/go-threads/common"
	"github.com/textileio/go-threads/db/keytransform"
	threadsutil "github.com/textileio/go-threads/util"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/api/server"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"gitlab.com/socialspecters.io/specters/pkg/util"
	"google.golang.org/grpc"
	"net"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"time"
)

// Version is the version of docdb-console
var Version = "No Version Provided"

var log = logging.Logger("specters")

var (
	debug       bool
	showVersion bool
)

func main() {
	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "SPCTRS", 0)

	repo := fs.String("repo", ".specters", "Repo location")
	hostAddrStr := fs.String("hostAddr", "/ip4/0.0.0.0/tcp/4006", "Libp2p host bind address")
	announceAddrStr := fs.String("announceAddr", "", "Libp2p announce address") // Should be supplied as multiaddr, /ip4/<your_public_ip>/tcp/4006
	apiAddrStr := fs.String("apiAddr", "/ip4/0.0.0.0/tcp/6006", "gRPC Specters API bind address")
	connLowWater := fs.Uint("connLowWater", 100, "Low watermark of libp2p connections that'll be maintained")
	connHighWater := fs.Uint("connHighWater", 400, "High watermark of libp2p connections that'll be maintained")
	connGracePeriod := fs.Duration("connGracePeriod", time.Second*20, "Duration a new opened connection is not subject to pruning")
	disableNetPulling := fs.Bool("disableNetPulling", false, "Disables automatic thread record and log pulling from network peers")
	netPullingLimit := fs.Uint("netPullingLimit", 10000, "Maximum number of records to request from network peers during a single pull (must be > 0)")
	netPullingStartAfter := fs.Duration("netPullingStartAfter", time.Second, "Delay after which thread pulling from network peers starts (must be > 0)")
	netPullingInitialInterval := fs.Duration("netPullingInitialInterval", time.Second, "Initial (first run) interval at which threads are pulled from network peers (must be > 0)")
	netPullingInterval := fs.Duration("netPullingInterval", time.Second*10, "Interval at which threads are pulled from network peers (must be > 0)")
	disableExchangeEdgesMigration := fs.Bool("disableExchangeEdgesMigration", false, "Disables automatic thread migration to the exchangeEdges protocol")
	enableNetPubsub := fs.Bool("enableNetPubsub", false, "Enables thread networking over libp2p pubsub")
	mongoURI := fs.String("mongoURI", "", "MongoDB URI (if not provided, an embedded Badger datastore will be used)")
	mongoDatabase := fs.String("mongoDatabase", "", "MongoDB database name (required with mongoURI")
	badgerLowMem := fs.Bool("badgerLowMem", false, "Use Badger's low memory settings")
	fs.BoolVar(&debug, "debug", false, "Enables debug logging")
	fs.BoolVar(&debug, "d", false, "Enables debug logging")
	fs.BoolVar(&showVersion, "version", false, "Print version information.")
	fs.BoolVar(&showVersion, "v", false, "Print version information.")
	if err := fs.Parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}

	if showVersion {
		fmt.Printf(`specters %s, Compiler: %s %s, Copyright (C) 2022 Social Specters.`,
			Version,
			runtime.Compiler,
			runtime.Version())
		fmt.Println()
		os.Exit(0)
	}

	hostAddr, err := multiaddr.NewMultiaddr(*hostAddrStr)
	if err != nil {
		log.Fatal(err)
	}
	var announceAddr multiaddr.Multiaddr
	if *announceAddrStr != "" {
		announceAddr, err = multiaddr.NewMultiaddr(*announceAddrStr)
		if err != nil {
			log.Fatal(err)
		}
	}
	apiAddr, err := multiaddr.NewMultiaddr(*apiAddrStr)
	if err != nil {
		log.Fatal(err)
	}

	var parsedMongoURI *url.URL
	if len(*mongoURI) != 0 {
		parsedMongoURI, err = url.Parse(*mongoURI)
		if err != nil {
			log.Fatalf("parsing mongoUri: %v", err)
		}
		if len(*mongoDatabase) == 0 {
			log.Fatal("mongoDatabase is required with mongoURI")
		}
	} else {
		log.Debugf("badgerLowMem: %v", *badgerLowMem)
	}

	err = logging.SetLogLevel("specters", util.LevelFromDebugFlag(debug))
	if err != nil {
		log.Fatal(err)
	}

	log.Debugf("repo: %v", *repo)
	log.Debugf("hostAddr: %v", *hostAddrStr)
	if announceAddr != nil {
		log.Debugf("announceAddr: %v", *announceAddrStr)
	}
	log.Debugf("apiAddr: %v", *apiAddrStr)
	log.Debugf("connLowWater: %v", *connLowWater)
	log.Debugf("connHighWater: %v", *connHighWater)
	log.Debugf("connGracePeriod: %v", *connGracePeriod)
	log.Debugf("enableNetPubsub: %v", *enableNetPubsub)
	if parsedMongoURI != nil {
		log.Debugf("mongoURI: %v", parsedMongoURI.Redacted())
		log.Debugf("mongoDatabase: %v", *mongoDatabase)
	} else {
		log.Debugf("badgerLowMem: %v", *badgerLowMem)
	}
	log.Debugf("debug: %v", debug)

	log.Info("==> Social Specters server configuration:")
	log.Info()
	log.Infof("        API Address: %v", *apiAddrStr)
	log.Infof("         Go Version: %v", runtime.Version())
	if *mongoURI != "" {
		log.Infof("            Storage: %v", *mongoURI)
	} else {
		log.Infof("            Storage: %v", *repo)
	}
	log.Infof("            Version: %v", Version)
	log.Infof("          Log Level: %v", util.LevelFromDebugFlag(debug))
	log.Info()
	log.Info("==> Welcome to Social Specters! Log data will stream in below:")

	opts := []common.NetOption{
		common.WithNetHostAddr(hostAddr),
		common.WithConnectionManager(connmgr.NewConnManager(int(*connLowWater), int(*connHighWater), *connGracePeriod)),
		common.WithNetPulling(
			*netPullingLimit,
			*netPullingStartAfter,
			*netPullingInitialInterval,
			*netPullingInterval,
		),
		common.WithNoNetPulling(*disableNetPulling),
		common.WithNoExchangeEdgesMigration(*disableExchangeEdgesMigration),
		common.WithNetPubSub(*enableNetPubsub),
		common.WithNetLogstore(common.LogstoreHybrid),
		common.WithNetDebug(debug),
	}
	if parsedMongoURI != nil {
		opts = append(opts, common.WithNetMongoPersistence(*mongoURI, *mongoDatabase))
	} else {
		opts = append(opts, common.WithNetBadgerPersistence(*repo))
	}
	if announceAddr != nil {
		opts = append(opts, common.WithAnnounceAddr(announceAddr))
	}
	n, err := common.DefaultNetwork(opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer func(n common.NetBoostrapper) {
		err := n.Close()
		if err != nil {

		}
	}(n)

	boostrapPeers, err := specters.DefaultBoostrapPeers()
	if err != nil {
		log.Fatal(err)
	}
	n.Bootstrap(boostrapPeers)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var store keytransform.TxnDatastoreExtended
	if *mongoURI != "" {
		store, err = mongods.New(ctx, *mongoURI, *mongoDatabase, mongods.WithCollName("eventstore"))
	} else {
		store, err = threadsutil.NewBadgerDatastore(*repo, "eventstore", *badgerLowMem)
	}
	if err != nil {
		log.Fatal(err)
	}

	dbInstance, err := db.NewThreadsDB(store, n, db.Config{
		Debug: debug,
	})
	if err != nil {
		log.Fatal(err)
	}

	service, err := specters.NewSpecters(
		context.Background(),
		n.Host().ID().String(),
		Version,
		dbInstance,
		specters.Config{Debug: debug},
	)
	if err != nil {
		log.Fatal(err)
	}

	s, err := server.NewServer(service, server.Config{
		Debug: debug,
	})
	if err != nil {
		log.Fatal(err)
	}

	target, err := util.TCPAddrFromMultiAddr(apiAddr)
	if err != nil {
		log.Fatal(err)
	}

	apiLogger := logging.Logger("specters:api").SugaredLogger.Desugar()
	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_ctxtags.StreamServerInterceptor(),
			grpc_zap.StreamServerInterceptor(apiLogger),
			server.StreamStatusInterceptor(service),
			grpc_recovery.StreamServerInterceptor(),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_zap.UnaryServerInterceptor(apiLogger),
			server.UnaryStatusInterceptor(service),
			grpc_recovery.UnaryServerInterceptor(),
		)),
	)
	listener, err := net.Listen("tcp", target)
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		pb.RegisterAPIServer(grpcServer, s)
		if err := grpcServer.Serve(listener); err != nil && !errors.Is(err, grpc.ErrServerStopped) {
			log.Fatalf("serve error: %v", err)
		}
	}()

	handleInterrupt(func() {
		stopped := make(chan struct{})
		go func() {
			grpcServer.GracefulStop()
			close(stopped)
		}()
		timer := time.NewTimer(10 * time.Second)
		select {
		case <-timer.C:
			grpcServer.Stop()
			fmt.Println("warn: server was shutdown ungracefully")
		case <-stopped:
			timer.Stop()
		}
		if err := dbInstance.Close(); err != nil {
			log.Fatal(err)
		}
		if err := n.Close(); err != nil {
			log.Fatal(err)
		}
	})
}

func handleInterrupt(stop func()) {
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info("Gracefully stopping... (press Ctrl+C again to force)")
	stop()
	os.Exit(1)
}
